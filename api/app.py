from flask import Flask, jsonify
from flask_cors import CORS
import sys
app = Flask(__name__)
cors = CORS(app)

@app.route('/')
def hello():
    return "Bravo, vous avez lancé votre container!!"

@app.route('/about')
def about():
	data = {
		"about": "Looks like it works",
		"python version": sys.version
	}
	return jsonify(data)

@app.route('/data')
def get_data():
	data = {
		"people": [
			{
				"firstname": "Jacky",
				"lastname": "Casas",
				"number": 42,
				"icon": "ti-crown",
				"type": "info"
			},
			{
				"firstname": "Nicolas",
				"lastname": "Boustany",
				"number": 3342,
				"icon": "ti-crown",
				"type": "danger"
			},
			{
				"firstname": "Leonardo",
				"lastname": "Angelini",
				"number": 9393,
				"icon": "ti-crown",
				"type": "success"
			},
			{
				"firstname": "Omar",
				"lastname": "Abou Khaled",
				"number": 444,
				"icon": "ti-crown",
				"type": "warning"
			}
		]
	}
	return jsonify(data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)