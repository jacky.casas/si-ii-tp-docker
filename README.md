SI-II - TP Docker
=================

Container Flask
---------------

Build the image for the Flask API and run it.

    cd api/
    docker build -t flask-api .
    docker run -p 5000:5000 flask-api

Then go to `http://localhost:5000` to see if the API works.

If you just want to run the Flask API locally:

    cd api/
    virtualenv -p /usr/bin/python3.x venv
    source venv/bin/activate
    pip install -r requirements.txt
    python app.py
    deactivate


Container Paper-Dashboard in VueJS
----------------------------------

Build and run the image.

    cd interface/
    docker build -t paper-dashboard .
    docker run -p 8080:8080 paper-dashboard

Then go to `http://localhost:8080` to see if the website works.

If you want to run the interface locally:

    cd interface/
    npm install
    npm run dev

NB: a HTTP request is made in file `src/pages/Dashboard.vue` to `http://localhost:5000/data` to load the data from the API.


Docker-compose
--------------

Run the two containers

    docker-compose up

See the [documentation of docker-compose](https://docs.docker.com/compose) if needed.